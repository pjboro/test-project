RELEASE_DESCRIPTION=$(curl --header "PRIVATE-TOKEN: $CI_JOB_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases/$CI_COMMIT_TAG" | jq ".description")
echo "$RELEASE_DESCRIPTION" | grep "**BROKEN**" && echo -e "\n\n\nTAG IS DENOTED AS BROKEN - DEPLOY ABORTED" && exit 1 
